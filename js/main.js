/*Funcion para controlar el evento onclick del boton submit de nuestro formulario.

Comenzamos definiendo un method de jQuery Validate para admitir solo letras en un input. @Copyright Carlos Seda Todos los derechos reservados.

Funcion validate estandar con rules para cada input y mensajes de error (personalizados sobreescribiendo la clase .error de jQuery Validate en css).

Si la validacion devulve true, mostramos una imagen sonriente, de lo contrario, mostramos una cara triste.
En ambos casos, se modifica la clase de la imagen contraria para asegurarnos de que esta oculta. Importante si el usuario intenta rellenar el formulario más de una vez.
*/

$(document).on("click","#submit", function(event){

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-záéíóúàèìòùäëïöüñ\s]+$/i.test(value);
    }, "Sólo se aceptan letras");
    
    event.preventDefault();

    var form = $("#register-form");

    form.validate({

        rules: {
            username:{
                required:true,
                lettersonly:true,
                minlength:2,
                maxlength:16
            },
            useremail:{
                required:true,
                email: true,
                maxlength:255
            }
        },

        messages:{
            username:{
                required: "Por favor, introduzca su nombre.",
                lettersonly: "El nombre solo puede contener letras.",
                minlength: "El nombre debe tener entre 2 y 16 caracteres.",
                maxlength: "El nombre debe tener entre 2 y 16 caracteres."
            },
            useremail:{
                required: "Por favor, introduzca su email.",
                email: "Debes introducir un correo electronico válido, como ejemplo@dominio.com",
                maxlength: "Su correo electronico es demasiado largo"
            }
        }
    });
    
    if(form.valid()){
        $("#smile-img").removeClass("hidden");
        $("#sad-img").addClass("hidden");
    }
    else{
        $("#sad-img").removeClass("hidden");
        $("#smile-img").addClass("hidden");
    }
});